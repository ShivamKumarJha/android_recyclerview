package info.androidhive.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Customer> customerList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CustomerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new CustomerAdapter(customerList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Customer customer = customerList.get(position);
                Toast.makeText(getApplicationContext(), customer.getCname() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareCustomerData();
    }

    private void prepareCustomerData() {
        Customer customer = new Customer("Amaresh Jana", "10", "2000");
        customerList.add(customer);

        customer = new Customer("Kakashi Aswin", "4", "5000");
        customerList.add(customer);

        customer = new Customer("Nitun Panigrahy", "5", "4000");
        customerList.add(customer);

        customer = new Customer("Shivam Kumar Jha", "007", "0");
        customerList.add(customer);

        customer = new Customer("Amaresh Jana", "10", "2000");
        customerList.add(customer);

        customer = new Customer("Kakashi Aswin", "4", "5000");
        customerList.add(customer);

        customer = new Customer("Nitun Panigrahy", "5", "4000");
        customerList.add(customer);

        customer = new Customer("Shivam Kumar Jha", "007", "0");
        customerList.add(customer);

        customer = new Customer("Amaresh Jana", "10", "2000");
        customerList.add(customer);

        customer = new Customer("Kakashi Aswin", "4", "5000");
        customerList.add(customer);

        customer = new Customer("Nitun Panigrahy", "5", "4000");
        customerList.add(customer);

        customer = new Customer("Shivam Kumar Jha", "007", "0");
        customerList.add(customer);

        customer = new Customer("Amaresh Jana", "10", "2000");
        customerList.add(customer);

        customer = new Customer("Kakashi Aswin", "4", "5000");
        customerList.add(customer);

        customer = new Customer("Nitun Panigrahy", "5", "4000");
        customerList.add(customer);

        customer = new Customer("Shivam Kumar Jha", "007", "0");
        customerList.add(customer);

        mAdapter.notifyDataSetChanged();
    }

}
