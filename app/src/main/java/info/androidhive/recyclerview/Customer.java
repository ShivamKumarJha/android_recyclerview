package info.androidhive.recyclerview;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Customer {
    private String cname, hours, amount;

    public Customer() {
    }

    public Customer(String cname, String hours, String amount) {
        this.cname = cname;
        this.hours = hours;
        this.amount = amount;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }
}
