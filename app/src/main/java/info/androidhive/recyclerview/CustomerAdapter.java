package info.androidhive.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {

    private List<Customer> customerList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cname, hours, amount;

        public MyViewHolder(View view) {
            super(view);
            cname = (TextView) view.findViewById(R.id.cname);
            hours = (TextView) view.findViewById(R.id.hours);
            amount = (TextView) view.findViewById(R.id.amount);
        }
    }


    public CustomerAdapter(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Customer customer = customerList.get(position);
        holder.cname.setText(customer.getCname());
        holder.hours.setText(customer.getHours());
        holder.amount.setText(customer.getAmount());
    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }
}
